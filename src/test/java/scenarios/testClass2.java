package scenarios;

import org.testng.annotations.Test;
import priority.Priority;

public class testClass2 {

    @Priority(3)
    @Test(description = "Test Multiple Day Absence Request flow", groups = "Test")
    public void Test3() {
        System.out.println("Test3");
    }

    @Priority(9)
    @Test(description = "Test Single Day Absence Request Flow", groups = "Regression")
    public void Test4() {
        System.out.println("Test4");
    }
}
