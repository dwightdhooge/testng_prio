package scenarios;

import org.testng.annotations.Test;
import priority.Priority;

public class testClass1 {

    @Priority(4)
    @Test(description = "Test Multiple Day Absence Request flow", groups = "Test")
    public void Test1() {
      System.out.println("Test1");
    }

    @Priority(8)
    @Test(description = "Test Single Day Absence Request Flow", groups = "Regression")
    public void Test2() {
        System.out.println("Test2");
    }
}
